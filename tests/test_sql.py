# coding: utf-8
# !/usr/bin/python3

import datetime as dt
import glob
import os
import pandas as pd
import pytest
import pyfygentlescrap_db as ba
import logging

from pyfygentlescrap_db.sql import columns_quotes
from sqlalchemy.exc import IntegrityError
from tests.create_testing_databases import create_test_database


def teardown_module(module):
    files = glob.glob("*.db", recursive=True)
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            logging.warning("Error: %s : %s" % (f, e.strerror))


@pytest.mark.parametrize(
    "df",
    [
        [["AAPL", "2020/12/01", 2, 4, 1, 3.0, 100, 0.0, 1.0]],
        [["AAPL", "2020-12-01", 2, 4.0, 1, 3, 100, 0.0, 1.0]],
        [["AAPL", "2020-12-01", "2", "4.", "1", "3", "100", "0.0", "1.0"]],
        [["AAPL", "2020, December 1st", "2", "4.", "1", "3", "100", "0.0", "1.0"]],
    ],
)
def test_pandas_conversions_from_raw(df):
    df = pd.DataFrame(df, columns=columns_quotes)
    # Check that convertion to exploitable format is successful:
    result = ba.sql._convert_dataframe_from_raw(df)
    assert result.dtypes["open"] == "float64"
    assert result.dtypes["high"] == "float64"
    assert result.dtypes["low"] == "float64"
    assert result.dtypes["close"] == "float64"
    assert result.dtypes["volume"] == "int64"
    assert result.dtypes["dividend"] == "float64"
    assert result.dtypes["split_ratio"] == "float64"
    assert result.loc["AAPL", "2020-12-01"]["open"] == 2.0
    for (ticker, day) in result.index:
        assert isinstance(ticker, str)
        assert ticker == "AAPL"
        assert isinstance(day, dt.datetime)
        assert day == dt.date(2020, 12, 1)


@pytest.mark.parametrize("set_index", [None, ["ticker"], ["day"], ["ticker", "day"]])
@pytest.mark.parametrize(
    "df",
    [
        [["AAPL", "2020/12/01", 2, 4, 1, 3.0, 100, 0.0, 1.0]],
        [["AAPL", "2020-12-01", 2, 4.0, 1, 3, 100, 0.0, 1.0]],
        [["AAPL", "2020-12-01", "2", "4.", "1", "3", "100", "0.0", "1.0"]],
        [["AAPL", "2020, December 1st", "2", "4.", "1", "3", "100", "0.0", "1.0"]],
    ],
)
def test_pandas_conversions_to_sql(df, set_index):
    expected = [["AAPL", "2020-12-01", "2.0", "4.0", "1.0", "3.0", "100", "0.0", "1.0"]]
    result = pd.DataFrame(df, columns=columns_quotes)
    if set_index is not None:
        result.set_index(set_index, inplace=True)
        result.sort_index(inplace=True)
    result = ba.sql._convert_dataframe_to_sql(result)
    assert (result.values == expected).all()


@pytest.mark.parametrize("invalid_filename", [True, 1, 1.0, None])
def test_invalid_database_parameter(invalid_filename):
    with pytest.raises(IOError):
        ba.get_db_connection(invalid_filename)


def test_creating_new_database():
    # create a new database, and check that the structure is correct.
    engine = ba.get_db_connection("database_that_do_not_exist.db")
    result = ba.sql._check_database(engine)
    assert result is True


@pytest.mark.parametrize("set_index", [None, ["ticker"], ["day"], ["ticker", "day"]])
@pytest.mark.parametrize(
    "dataframe, expected",
    [
        (
            [
                ["AAPL", "2020/12/01", 2, 4, 1, 3, 100, 0.0, 1.0],
                ["AAPL", "2020/12/02", 12, 14, 11, 13, 1000, 0.0, 1.0],
            ],
            2,
        ),
        (
            [
                ["AAPL", "2020-12-01", 2, 4, 1, 3, 100, 0.0, 1.0],
                ["AAPL", "2020/12/02", 12, 14, 11, 13, 1000, 0.0, 1.0],
            ],
            2,
        ),
        (
            [
                ["AAPL", "2020/12/01", 2, 4, 1, 3, 100, 0.0, 1.0],
                ["AAPL", "2020/12/02", 12, 14, 11, 13, 1000, 0.0, 1.0],
                ["TSLA", "2020/12/01", 102, 104, 101, 103, 10100, 0.0, 1.0],
                ["TSLA", "2020/12/02", 112, 114, 111, 113, 11000, 0.0, 1.0],
            ],
            4,
        ),
    ],
)
def test_push_to_a_new_database(set_index, dataframe, expected):
    engine = ba.get_db_connection("new_database.db")
    df = pd.DataFrame(dataframe, columns=columns_quotes)
    if set_index is not None:
        df.set_index(set_index, inplace=True)
        df.sort_index(inplace=True)
    result = ba.push(engine, df)
    os.remove("new_database.db")
    assert result["new_lines"] == expected
    assert result["updated_lines"] == 0


@pytest.mark.parametrize(
    "day_fmt1",
    ["2020-12-01", "2020/12/01", dt.datetime(2020, 12, 1), dt.date(2020, 12, 1)],
)
@pytest.mark.parametrize(
    "day_fmt2",
    ["2020-12-01", "2020/12/01", dt.datetime(2020, 12, 1), dt.date(2020, 12, 1)],
)
def test_sql_unique_constraint_fail(day_fmt1, day_fmt2):
    engine = ba.get_db_connection("new_database.db")
    df = [  # Two cotations for the same couple ticker/day:
        ["AAPL", day_fmt1, 2, 4, 1, 3, 100, 0.0, 1.0],
        ["AAPL", day_fmt2, 12, 14, 11, 13, 1000, 0.0, 1.0],
    ]
    df = pd.DataFrame(df, columns=columns_quotes)
    with pytest.raises(IntegrityError):
        ba.push(engine, df)
    os.remove("new_database.db")


@pytest.mark.parametrize("date1", ["2018/01/02", "2018-01-02"])
@pytest.mark.parametrize("date2", ["2018/01/02", "2018-01-02"])
def test_push_to_existing_date(date1, date2):
    engine = ba.get_db_connection("new_database.db")
    dataframe1 = ["AAPL", date1, 2, 4, 1, 3, 100, 0.0, 1.0]
    dataframe2 = ["AAPL", date2, 2, 4, 1, 3, 100, 0.0, 1.0]
    df = pd.DataFrame([dataframe1, dataframe2], columns=columns_quotes)
    ba.push(engine, df)
    df = ba.pull(engine, "AAPL", date1)
    assert len(df) == 1
    df = ba.pull(engine, "AAPL", date2)
    assert len(df) == 1
    os.remove("new_database.db")


@pytest.mark.parametrize(
    "df",
    [
        [
            ["AAPL", "2018/01/02", 1, 2, 3, 4, 100, 0.0, 1.0],
            ["AAPL", "2018/01/03", 5, 6, 7, 8, 101, 0.0, 1.0],
        ],
        [
            ["AAPL", "2018/01/02", 1, 2, 3, 4, 100, 0.0, 1.0],
            ["TSLA", "2019/12/30", 5, 6, 7, 8, 101, 0.0, 1.0],
            ["TSLA", "2019/12/27", 9, 10, 11, 12, 102, 0.0, 1.0],
        ],
    ],
)
def test_update_database(df):
    engine = create_test_database("test.db")
    result = ba.push(engine, df)
    assert result["new_lines"] == 0
    assert result["updated_lines"] == len(df)

    df = pd.DataFrame(df, columns=columns_quotes)
    expected = ba.sql._convert_dataframe_from_raw(df)
    tickers = expected.index.get_level_values(level="ticker").unique().tolist()
    result = ba.pull(engine, tickers=tickers, day="*")
    result = result.loc[expected.index.intersection(expected.index)]
    assert (result == expected).all().all()
    os.remove("test.db")


class TestDatabasePull:
    def setup_class(self):
        self.engine = create_test_database("test_pull.db")

    def teardown_class(self):
        os.remove("test_pull.db")

    @pytest.mark.parametrize(
        "ticker, dates, expected",
        [
            ("AAPL", [True, False, 1, 1.0, None], 0),
            ("AAPL", ["2018-01-01", "2018/01/01", "not_a_valid_day", "1:2"], 0),
            ("AAPL", ["2018-01-02", "2018/01/02"], 1),
            ("AAPL", ["2018/01/02:2018/01/02", "2018-01-02:2018-01-02"], 1),
            ("AAPL", [dt.datetime(2018, 1, 2), dt.date(2018, 1, 2)], 1),
            ("AAPL", ["2019:2019-12-31", "2019-01-01:2019-12-31"], 252),
            ("AAPL", [":", "*", "*:*"], 503),
            ("*", [True, False, 1, 1.0, None], 0),
            ("*", ["2018-01-01", "2018/01/01", "not_a_valid_day", "1:2"], 0),
            ("*", ["2018-01-02", "2018/01/02"], 1),
            ("*", ["2018/01/02:2018/01/02", "2018-01-02:2018-01-02"], 1),
            ("*", [dt.datetime(2018, 1, 2), dt.date(2018, 1, 2)], 1),
            ("*", ["2019:2019-12-31", "2019-01-01:2019-12-31"], 272),
            ("*", [":", "*", "*:*"], 523),
            ("ZZZZ", [True, False, 1, 1.0, None], 0),
            ("ZZZZ", ["2018-01-01", "2018/01/01", "not_a_valid_day", "1:2"], 0),
            ("ZZZZ", ["2018-01-02", "2018/01/02"], 0),
            ("ZZZZ", ["2018/01/02:2018/01/02", "2018-01-02:2018-01-02"], 0),
            ("ZZZZ", [dt.datetime(2018, 1, 2), dt.date(2018, 1, 2)], 0),
            ("ZZZZ", ["2019:2019-12-31", "2019-01-01:2019-12-31"], 0),
            ("ZZZZ", [":", "*", "*:*"], 0),
            ("TSLA", [True, False, 1, 1.0, None], 0),
            ("TSLA", ["2018-01-01", "2018/01/01", "not_a_valid_day", "1:2"], 0),
            ("TSLA", ["2018-01-02", "2018/01/02"], 0),
            ("TSLA", ["2018/01/02:2018/01/02", "2018-01-02:2018-01-02"], 0),
            ("TSLA", [dt.datetime(2018, 1, 2), dt.date(2018, 1, 2)], 0),
            ("TSLA", ["2019:2019-12-31", "2019-01-01:2019-12-31"], 20),
            ("TSLA", [":", "*", "*:*"], 20),
        ],
    )
    def test_pull_from_database(self, ticker, dates, expected):
        for date in dates:
            result = ba.pull(self.engine, ticker, date)
            assert len(result) == expected

    @pytest.mark.parametrize(
        "ticker, date, expected",
        [
            ("AAPL", "2018/01/02", [[42.54, 43.08, 42.31, 43.06, 102223600]]),
            ("AAPL", "2018-01-02", [[42.54, 43.08, 42.31, 43.06, 102223600]]),
            ("AAPL", "2018-02-09", [[39.27, 39.47, 37.56, 39.10, 282690400]]),
            ("AAPL", "2018-02-09", [[39.27, 39.47, 37.56, 39.10, 282690400]]),
            ("TSLA", "2019-12-27", [[87.00, 87.06, 85.22, 86.08, 49728500]]),
        ],
    )
    def test_pull_from_database2(self, ticker, date, expected):
        df = ba.pull(self.engine, ticker, date)
        df = df.loc[ticker][["open", "high", "low", "close", "volume"]]
        result = df.values.tolist()
        assert result == expected

    @pytest.mark.parametrize(
        "period, expected",
        [
            (
                "2019-01-01:2019-12-31",
                ["2019-02-08", "2019-05-10", "2019-08-09", "2019-11-07"],
            ),
            (
                "2018-01-01:2018-12-31",
                ["2018-02-09", "2018-05-11", "2018-08-10", "2018-11-08"],
            ),
            ("2020:2021", []),
        ],
    )
    def test_pull_dividends(self, period, expected):
        df = ba.pull(self.engine, "*", period)
        df = df[df.dividend > 0.0]
        result = df.index.get_level_values(level="day").values
        expected = pd.to_datetime(expected)
        assert (result == expected).all()
