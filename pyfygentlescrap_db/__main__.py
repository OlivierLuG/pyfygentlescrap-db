# coding: utf-8
# !/usr/bin/python3

"""
BourseAuto_database main script.

Args:
    --database: Path to the SQLlite database (default value is
        ``database.sqlite``).
    --yahoo-region [region1, region2, ...]: Region or list of regions
        to fetch EOD data (or delayed data if the market is open). The
        source comes from https://finance.yahoo.com/screener/new.
    -y --ticker [tick1 tick2 ...]: List of tickers for historical EOD
        values download. By default, 100 lines are fetched (from today
        back to 100 days before). The date period can be modified by
        providing ``--day-from`` and/or ``--day-to`` parameters.
    --day-from: Define the day of the first value to fetch data. If
        --day-from is set, without --day-to
    --day-to: Define the day of the last value to fetch data.
    --log-file: File to store the logging output
    --log-file-level: Set the logging level for the log-file provided.
        The levels are: ``DEBUG`` (default) / ``INFO`` / ``WARNING`` /
        ``ERROR`` / ``CRITICAL``.
    --log-stdout-level: Set the logging level for the terminal.
        The levels are: ``DEBUG`` (default) / ``INFO`` / ``WARNING`` /
        ``ERROR`` / ``CRITICAL``.
"""

import argparse
import pyfygentlescrap_db as ba
import datetime as dt
import logging
import pyfygentlescrap as pfgs
import pandas as pd
import pytz

log_level = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "critical": logging.CRITICAL,
}


def _parsing_arguments():
    """ Parse arguments passed to the script. """
    parser = argparse.ArgumentParser(description="Bourse AUTO 2020.")
    parser.add_argument(
        "--database",
        nargs=1,
        metavar="db_path",
        default="database.sqlite",
        help="Path to an existing database. Create a database if the file "
        "does not exist.",
    )
    parser.add_argument(
        "--yahoo-region",
        nargs="+",
        metavar="yahoo_region",
        help="List of Yahoo tickers to download.",
    )
    parser.add_argument(
        "--ticker",
        nargs="+",
        metavar="yahoo_ticker",
        help="List of Yahoo tickers to download.",
    )
    parser.add_argument(
        "--day-from",
        nargs=1,
        metavar="day_from",
        help="Starting date for EOD downloads.",
    )
    parser.add_argument(
        "--day-to",
        nargs=1,
        metavar="day_to",
        help="Finishing date for EOD downloads.",
    )
    parser.add_argument(
        "--logfile",
        nargs=1,
        metavar="logfile",
        default="log.txt",
        help="Path to the log file.",
    )
    parser.add_argument(
        "--logfile-level",
        nargs=1,
        metavar="logfile_level",
        default="DEBUG",
        help="Logging level for the logile.",
    )
    parser.add_argument(
        "--log-stdout-level",
        nargs=1,
        metavar="stdout_level",
        default="WARNING",
        help="Logging level for the terminal.",
    )
    options = parser.parse_args()
    return options


def _set_debbuging_options(stdout_level, logfile, logfile_level):
    logger = logging.getLogger("bourseauto_database")
    fmt = logging.Formatter(
        "%(asctime)s %(name)s.%(funcName)s %(levelname)s: %(message)s",
        datefmt="%Y/%m/%d %H:%M:%S",
    )
    # defintion of stdout logging level:
    ch = logging.StreamHandler()
    ch.setFormatter(fmt)
    ch.setLevel(log_level[stdout_level.lower()])
    logger.addHandler(ch)
    # definition for logfile:
    fh = logging.FileHandler(logfile)
    fh.setFormatter(fmt)
    fh.setLevel(log_level[logfile_level.lower()])
    logger.addHandler(fh)
    logger.setLevel(log_level[logfile_level.lower()])
    # log pyfygentlescrap to the logfile:
    logger = logging.getLogger("pyfygentlescrap")
    logger.addHandler(fh)
    logger.setLevel(log_level[logfile_level.lower()])


def main():
    # Parsing arguments:
    argvs = _parsing_arguments()
    database = argvs.database
    yahoo_region = argvs.yahoo_region
    ticker = argvs.ticker
    day_from = argvs.day_from
    day_to = argvs.day_to

    # Setting the logging levels and outputs:
    _set_debbuging_options(argvs.log_stdout_level, argvs.logfile, argvs.logfile_level)

    # Initialization:
    logger = logging.getLogger("bourseauto_database")
    fmt = "%Y-%m-%d, %H:%M:%S %Z"
    time_local = dt.datetime.now().strftime(fmt)
    time_NY = dt.datetime.now(pytz.timezone("America/New_York")).strftime(fmt)
    time_London = dt.datetime.now(pytz.timezone("Europe/London")).strftime(fmt)
    time_Paris = dt.datetime.now(pytz.timezone("Europe/Paris")).strftime(fmt)
    logger.info(
        f"Script started. Version {ba.__version__}.\n"
        f"                    Arguments: database path:      {database}\n"
        f"                               yahoo_region:       {yahoo_region}\n"
        f"                               ticker:             {ticker}\n"
        f"                               day_from:           {day_from}\n"
        f"                               day_to:             {day_to}\n"
        f"                    Main modules: pyfygentlescrap: v{pfgs.__version__}\n"
        f"                                  pandas:          v{pd.__version__}\n"
        f"                    Time: local:                   {time_local}\n"
        f"                          America/New_York:        {time_NY}\n"
        f"                          Europe/London:           {time_London}\n"
        f"                          Europe/Paris:            {time_Paris}"
    )

    # Connection to the database:
    con = ba.get_db_connection(database)

    # Openning a Yahoo session:
    session = pfgs.yahoo_session()
    if yahoo_region is not None:
        for region in yahoo_region:
            df = session.yahoo_equity_screener(region=region)
            df2 = ba.yahoo_equity_screener_conversion(df)
            print(df2)
            print(
                df2.index.get_level_values(level="day").min(),
                df2.index.get_level_values(level="day").max(),
            )
            print(df2[df2.isna().any(axis=1)])
            ba.push(con, df2)

    # Exiting script:
    logger.info("Script finished successfully.")


if __name__ == "__main__":
    main()
