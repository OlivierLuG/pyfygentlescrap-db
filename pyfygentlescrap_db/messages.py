# coding: utf-8
# !/usr/bin/python3
""" Standard messages. """


def invalid_dataframe(df):
    return f"The DataFrame:\n{df}\nis not valid."


def database_does_not_exist(filename):
    return f"Database {filename} does not exist. A new database will be created."


def wrong_parameter_type(parameter, expected_type):
    return (
        f"Parameter {parameter}, type {type(parameter)}, "
        f"must be of type {expected_type}."
    )


def wrong_parameter_format(param, expected_fmt=None):
    msg_fmt = "" if expected_fmt is None else f" Expected format is: {expected_fmt}."
    return f"Parameter {param} does not have a valid format.{msg_fmt}"


def dataframe_unique_problem(df):
    return f"The dataframe:\n{df}\ncontains duplicate values."
