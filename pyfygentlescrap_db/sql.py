# coding: utf-8
# !/usr/bin/python3

import datetime as dt
import logging
import os
import pandas as pd
import sqlalchemy as db

from pyfygentlescrap_db import messages as msg
from sqlalchemy.exc import NoSuchTableError

columns_quotes = [
    "ticker",
    "day",
    "open",
    "high",
    "low",
    "close",
    "volume",
    "dividend",
    "split_ratio",
]
logger = logging.getLogger(__name__)


def _void_dataframe():
    df = pd.DataFrame([], columns=columns_quotes)
    df.set_index(["ticker", "day"], inplace=True)
    df.sort_index(inplace=True)
    return df


def _convert_dataframe_from_raw(df):
    df.reset_index(inplace=True)
    df = df[columns_quotes]
    df = df.astype(
        {
            "ticker": "string",
            "day": "datetime64",
            "open": "float64",
            "high": "float64",
            "low": "float64",
            "close": "float64",
            "volume": "int64",
            "dividend": "float64",
            "split_ratio": "float64",
        }
    )
    df.set_index(["ticker", "day"], inplace=True)
    df.sort_index(inplace=True)
    return df


def _convert_dataframe_to_sql(df):
    df = _convert_dataframe_from_raw(df)
    df.reset_index(inplace=True)
    return df.astype(
        {
            "ticker": "string",
            "day": "string",
            "open": "string",
            "high": "string",
            "low": "string",
            "close": "string",
            "volume": "string",
            "dividend": "string",
            "split_ratio": "string",
        }
    )


def _check_database(engine):
    """
    Check that the ``engine`` contains a database with a valid structure.
    """
    # Check that the table 'quotes' exists:
    try:
        quotes = db.Table("quotes", db.MetaData(), autoload=True, autoload_with=engine)
    except NoSuchTableError:
        return False
    # Check that the columns 'day', 'open', ... exist:
    query = db.select([quotes])
    ResultProxy = engine.execute(query)
    ResultSet = ResultProxy.fetchall()
    df = pd.DataFrame(ResultSet, columns=ResultProxy._metadata.keys)
    if len(set(df.columns) & set(columns_quotes)) == 9:
        return True
    return False


def _create_new_database(filename):
    """ Create a new SQLite database."""
    engine = db.create_engine("sqlite:///" + filename, echo=True)
    meta = db.MetaData()
    db.Table(
        "quotes",
        meta,
        db.Column("ticker", db.String(40), primary_key=True),
        db.Column("day", db.String, primary_key=True),
        db.Column("open", db.String),
        db.Column("high", db.String),
        db.Column("low", db.String),
        db.Column("close", db.String),
        db.Column("volume", db.String),
        db.Column("dividend", type_=db.String, default="0."),
        db.Column("split_ratio", type_=db.String, default="1."),
    )
    meta.create_all(engine)


def get_db_connection(filename):
    """
    Establish the connection to the database.

    Args:
        ``filename`` (str): absolute or relative path to the database.

    Returns:
        database connection (sqlalchemy engine).

    Raises:
        ``IOError``: if filename is not found.
    """
    if type(filename) is not str:
        raise IOError(msg.wrong_parameter_type(filename, str))
    if not os.path.isfile(filename):
        logger.warning(msg.database_does_not_exist(filename))
        _create_new_database(filename)
        return get_db_connection(filename)

    engine = db.create_engine("sqlite:///" + filename)
    return engine.connect()


def push(engine, df):
    """
    Push DataFrame values to the database. If some data already exists,
    then the database is updated.

    Args:
        engine: database engine connection.
        df: ``DataFrame`` containing the values to push.

    Returns:
        Number of tickers that were added to thedatabase.

    Raises:
        bla: text
    """

    if isinstance(df, list):
        df = pd.DataFrame(df, columns=columns_quotes)

    df = _convert_dataframe_from_raw(df)

    if not df.index.is_unique:
        logger.warning(
            msg.dataframe_unique_problem(df) + " Duplicates values will be dropped."
        )
        df = df.drop_duplicates()
    tickers = df.index.get_level_values(level="ticker").unique().tolist()

    # Connection with the table ``quotes``, and fetching all existing data:
    quotes = db.Table("quotes", db.MetaData(), autoload=True, autoload_with=engine)
    day_interval = (
        df.index.get_level_values(level="day").min().strftime("%Y-%m-%d")
        + ":"
        + df.index.get_level_values(level="day").max().strftime("%Y-%m-%d")
    )
    existing_quotes = pull(engine, tickers, day_interval)

    # New data are directly inserted data without any check:
    new_lines = df.loc[df.index.difference(existing_quotes.index)]
    new_lines = _convert_dataframe_to_sql(new_lines)
    if len(new_lines) > 0:
        engine.execute(quotes.insert(), new_lines.to_dict(orient="records"))

    # Existing quotes are updated without any check:
    updated_lines = df.loc[df.index.intersection(existing_quotes.index)]
    updated_lines = _convert_dataframe_to_sql(updated_lines)
    for idx in updated_lines.index:
        values = updated_lines.iloc[idx].to_dict()
        query = (
            quotes.update()
            .where(
                db.sql.and_(
                    quotes.columns.ticker == updated_lines.iloc[idx].ticker,
                    quotes.columns.day == updated_lines.iloc[idx].day,
                )
            )
            .values(values)
        )
        engine.execute(query)

    return {"new_lines": len(new_lines), "updated_lines": len(updated_lines)}


def pull(engine, tickers, day):
    """
    Downloads data from the database. This function is written so no error is
    raised. If an error occurs, the returned value is a void DataFrame.

    Args:
        engine: database engine connection. See ``get_db_connection``.
        ticker: list of tickers (``['^FCHI', '^GSPC', ...]``) or single ticker
            as string (``'^FCHI'``).
        days: single day as `datetime` or `str` or range of days as `str`. See
            examples for more details.

    Returns:
        DataFrame with columns ``[['day', 'open', 'high', ... ]]`` over the day
        or range of day.

    Examples:
        ``pull(engine, '^FCHI', '2020-05-02')``
            Returns the values for the ticker ^FCHI at the requested date.
        ``pull(engine, '[^FCHI', '^GSPC'], '2020-05-02')``
            Returns the values for the two tickers at the requested date.
        ``pull(engine, '[^FCHI'], '2019-01-01:2019-12-31')``
            Returns the values for the ticker ^FCHI for the year 2019.
    """
    # Parsing tickers parameter:
    if not isinstance(tickers, list):
        tickers = [tickers]
    for ticker in tickers:
        if type(ticker) is not str:
            logger.warning(msg.wrong_parameter_type(ticker, str))
            return _void_dataframe()

    # Parsing day parameter:
    if isinstance(day, str):
        txt = day.split(":")
        if day == "*" or day == ":":
            day_from = "1677-09-22"
            day_to = "2262-04-11"
        elif len(txt) == 1:
            try:
                day = pd.to_datetime(day)
            except ValueError:
                expected_format = "'Y-%m-%d' or '%Y/%m/%d' or 'Y-%m-%d:Y-%m-%d'"
                logger.warning(msg.wrong_parameter_format(day, expected_format))
                return _void_dataframe()
            else:
                day_from = day.strftime("%Y-%m-%d")
                day_to = day_from
        else:
            try:
                if txt[0] == "*" or txt[0] == ":":
                    day_from = dt.datetime(1677, 9, 22)
                else:
                    day_from = pd.to_datetime(txt[0])
                if txt[1] == "*" or txt[1] == ":":
                    day_to = dt.datetime(2262, 4, 11)
                else:
                    day_to = pd.to_datetime(txt[1])
            except ValueError:
                expected_format = "'Y-%m-%d' or '%Y/%m/%d' or 'Y-%m-%d:Y-%m-%d'"
                logger.warning(msg.wrong_parameter_format(day, expected_format))
                return _void_dataframe()
            else:
                if day_from > day_to:
                    (day_from, day_to) = (day_to, day_from)
                day_from = day_from.strftime("%Y-%m-%d")
                day_to = day_to.strftime("%Y-%m-%d")
    elif isinstance(day, (dt.date, dt.datetime)):
        day_from = day.strftime("%Y-%m-%d")
        day_to = day_from
    else:
        logger.warning(msg.wrong_parameter_type(ticker, str))
        return _void_dataframe()

    # connection to the table ``quotes``:
    try:
        quotes = db.Table("quotes", db.MetaData(), autoload=True, autoload_with=engine)
        query = db.select([quotes])
        if tickers != ["*"]:
            query = query.where(quotes.columns.ticker.in_(tickers))
        query = query.where(quotes.columns.day >= day_from).where(
            quotes.columns.day <= day_to
        )
        ResultProxy = engine.execute(query)
        ResultSet = ResultProxy.fetchall()
    except (AttributeError, NoSuchTableError):
        logger.warning(msg.database_does_not_exist("quotes"))
        return _void_dataframe()

    # creating and returning the DataFrame:
    df = pd.DataFrame(ResultSet, columns=ResultProxy._metadata.keys)
    return _convert_dataframe_from_raw(df)
