# coding: utf-8
# !/usr/bin/python3

from pyfygentlescrap_db.sql import columns_quotes
from pyfygentlescrap.yahoo.yahoo_shared import _timestamp_to_datetime


def _convert_date(x):
    return _timestamp_to_datetime(x, tzone="UTC")


def yahoo_equity_screener_conversion(df):
    df.reset_index(inplace=True)
    df.rename(
        columns={
            "symbol": "ticker",
            "regularMarketTime": "day",
            "regularMarketOpen": "open",
            "regularMarketDayHigh": "high",
            "regularMarketDayLow": "low",
            "regularMarketPrice": "close",
            "regularMarketVolume": "volume",
        },
        inplace=True,
    )
    df["day"] = df["day"].apply(_convert_date)
    df["dividend"] = 0.0
    df["split_ratio"] = 1.0
    df = df[columns_quotes]
    values = {"open": 0.0, "high": 0.0, "low": 0.0, "close": 0.0, "volume": 0}
    df.fillna(values, inplace=True)
    df.set_index(["ticker", "day"], inplace=True)
    df.sort_index(inplace=True)
    return df
